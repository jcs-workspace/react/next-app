'use client';
import React from 'react';

interface Props {
    error: Error;
    reset: () => void;
}

const ErrorPage = (props: Props) => {
    console.log('Error', props.error);

    return (
        <>
        <div>An unexpected error has occurred.</div>
        <button className='btn' onClick={() => props.reset()}>Retry</button>
        </>
    );
}

export default ErrorPage;
