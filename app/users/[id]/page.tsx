import { notFound } from 'next/navigation';
import React from 'react';

interface Props {
    params: { id: number }
}

const UserDetialPage = ({ params: { id } }: Props) => {
    if (id > 10) notFound();  // manually call not found.
    return (
        <div>User Detail Page {id}</div>
    );
};

export default UserDetialPage;
