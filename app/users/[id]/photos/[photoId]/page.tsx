import React from 'react';

interface Props {
    params: { id: number, photoId: number, }
}

const PhotoPage = (props: Props) => {
    return (
        <div>Photo Page {props.params.id} and {props.params.photoId}</div>
    );
};

export default PhotoPage;
